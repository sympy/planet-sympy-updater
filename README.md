This updater takes the docker image created by
https://github.com/sympy/planet-sympy/, and runs it. The results are pushed
into https://github.com/planet-sympy/planet.sympy.org/tree/gh-pages.

This requires GitLab Runner using Docker Executor in Privileged mode.

```
[[runners]]
  name = "runner"
  url = "https://gitlab.com/ci"
  token = "TOKEN"
  executor = "docker"
  [runners.docker]
    privileged = true
```

We use the gitlab.com shared runner, which runs in a privileged mode.

The run is triggered every 20 minutes by a Trigger (Settings->CI/CD
Pipelines->Pipeline triggers->Add Trigger), which is run from a cron job every 20 min:

    crontab -e
    */20 * * * * https_proxy=HTTPS_PROXY curl -X POST -F token=TOKEN -F ref=master https://gitlab.com/api/v4/projects/1525395/trigger/pipeline >> /home/USERNAME/tmp/cron.log 2>&1

(Change HTTPS_PROXY, TOKEN and USERNAME.)
